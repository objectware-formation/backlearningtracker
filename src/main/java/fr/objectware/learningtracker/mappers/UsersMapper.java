package fr.objectware.learningtracker.mappers;

import fr.objectware.learningtracker.controllers.users.dto.UsersDtoUpdate;
import fr.objectware.learningtracker.models.UsersModel;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;

@Mapper(componentModel = "spring")
public interface UsersMapper {

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    void updateUserFromDto(UsersDtoUpdate userDto, @MappingTarget UsersModel entity);
}
