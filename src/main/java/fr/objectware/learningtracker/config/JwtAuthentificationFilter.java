package fr.objectware.learningtracker.config;

import fr.objectware.learningtracker.config.enums.Routes;
import fr.objectware.learningtracker.models.UsersModel;
import fr.objectware.learningtracker.services.UserDetailsService;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;

@Component
public class JwtAuthentificationFilter extends OncePerRequestFilter {

    @Autowired
    private JwtTokenUtils JwtUtils;
    @Autowired
    private UserDetailsService users;
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        if(Routes.isSecurized(request.getContextPath())){
            filterChain.doFilter(request, response);
        }


        String header = request.getHeader("Authorization");
        String jwt;
        String userMail;

        if(header == null || !header.startsWith("Bearer ")){
            filterChain.doFilter(request, response);
            return;
        }

        jwt = header.substring(7);
        userMail = JwtUtils.getUsernameFromToken(jwt);


        if(userMail != null && SecurityContextHolder.getContext().getAuthentication() == null) {
            UsersModel userDetails = this.users.loadUserByUsername(userMail);
            boolean isValidToken = JwtUtils.validateToken(jwt, userDetails);

            if(isValidToken) {
                UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(
                        userDetails,
                        null,
                        userDetails.getAuthorities()
                );

                authToken.setDetails(
                        new WebAuthenticationDetailsSource().buildDetails(request)
                );

                SecurityContextHolder.getContext().setAuthentication(authToken);
            }
            filterChain.doFilter(request, response);
        }

    }
}
