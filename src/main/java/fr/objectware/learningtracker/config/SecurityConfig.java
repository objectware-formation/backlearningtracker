package fr.objectware.learningtracker.config;

import fr.objectware.learningtracker.LearningTrackerApplication;
import fr.objectware.learningtracker.config.enums.Routes;
import fr.objectware.learningtracker.errors.UserAuthenticationErrorHandler;
import fr.objectware.learningtracker.errors.UserForbiddenErrorHandler;
import fr.objectware.learningtracker.services.UserDetailsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
public class SecurityConfig {

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private JwtAuthentificationFilter jwtAuthentificationFilter;

    @Bean
    @Order(1)
    public SecurityFilterChain bookFilterChain(HttpSecurity http) throws Exception {

        Logger logger = LoggerFactory.getLogger(LearningTrackerApplication.class);

        http.
                csrf().
                disable().
                sessionManagement(session -> session.sessionCreationPolicy(SessionCreationPolicy.STATELESS)).
                httpBasic().
                and().
                addFilterBefore(jwtAuthentificationFilter, UsernamePasswordAuthenticationFilter.class).
                exceptionHandling(exception -> exception
                        .authenticationEntryPoint(userAuthenticationErrorHandler())
                        .accessDeniedHandler(new UserForbiddenErrorHandler()));

        for(Routes route : Routes.values()) {
            System.out.println("Route instanciation : Name : " + route.name() + ", Path : " + route.getPath());
            if(route.isSecurized()) {
                if(route.hasRole()) {
                    System.out.println("Route role securization : Name : " + route.name() + ", Path : " + route.getPath());

                    http.authorizeHttpRequests().requestMatchers(route.getPath()).hasRole(route.getRole());
                }
                System.out.println("Route auth securization : Name : " + route.name() + ", Path : " + route.getPath());

                http.authorizeHttpRequests().requestMatchers(route.getPath()).authenticated();
            }else {
                System.out.println("Route no securization : Name : " + route.name() + ", Path : " + route.getPath());
                http.authorizeHttpRequests().requestMatchers(route.getPath()).permitAll();
            }
        }

        return http.build();
    }


    @Bean
    public AuthenticationEntryPoint userAuthenticationErrorHandler() {
        UserAuthenticationErrorHandler userAuthenticationErrorHandler =
                new UserAuthenticationErrorHandler();
        userAuthenticationErrorHandler.setRealmName("Basic Authentication");
        return userAuthenticationErrorHandler;
    }

    @Bean
    @Order(3)
    public DaoAuthenticationProvider authProvider() {
        DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
        authProvider.setUserDetailsService(userDetailsService);
        authProvider.setPasswordEncoder(encoder());
        return authProvider;
    }

    @Bean
    @Order(2)
    public PasswordEncoder encoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    @Order(4)
    public AuthenticationManager authenticationManager(
            AuthenticationConfiguration configuration) throws Exception {
        return configuration.getAuthenticationManager();
    }
}