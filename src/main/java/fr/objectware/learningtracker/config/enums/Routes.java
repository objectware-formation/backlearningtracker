package fr.objectware.learningtracker.config.enums;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public enum Routes {
    DEFAULT("/", true, "DEFAULT"),
    AUTH("/auth/**", false, null),
    FETCH_BOOKS("/books", false, null),
    REPORTS("/reports/**",false,null),
    FETCH_USERS("/users", true, "DEFAULT"),
    FETCH_USER("/users/find/{uuid}", true, "DEFAULT"),
    FETCH_USERNAME_BY_UUID("/users/name/{uuid}", true, "DEFAULT"),
    UPDATE_TAGS("/books/update-tags/{uuid}", true, "DEFAULT"),
    REGISTER_BOOK("/books/create", true, "DEFAULT");

    String path;
    boolean securized;
    String role;

    Routes(String path, boolean securized, String role){
        this.path = path;
        this.securized = securized;
        this.role = role;
    }

    public String getPath() {
        return path;
    }

    public boolean isSecurized() {
        return securized;
    }

    public static boolean isSecurized(String path) {
        for(Routes routes : Routes.values()) {
            if(routes.getPath().equals(path)) {
                return routes.isSecurized();
            }
        }

        return false;
    }

    public String getRole() {
        return role;
    }

    public boolean hasRole() {
        return this.role == null;
    }
}
