package fr.objectware.learningtracker.errors;

import org.springframework.http.HttpStatus;

public enum ErrorType {
    USER_NOT_FOUND("User not found", HttpStatus.NOT_FOUND.value()),
    USER_ALREADY_EXISTS("User already exists", HttpStatus.CONFLICT.value()),
    USER_NOT_AUTHORIZED("User not authorized", HttpStatus.UNAUTHORIZED.value()),
    CONNEXION_REFUSED("Connexion refused", HttpStatus.UNAUTHORIZED.value()),
    CREDENTIALS_INVALID("Credentials invalid", HttpStatus.UNAUTHORIZED.value()),
    DB_ERROR_CREATION("Error : user not created", HttpStatus.SERVICE_UNAVAILABLE.value()),
    DB_ERROR_UPDATE("Error : user not updated", HttpStatus.SERVICE_UNAVAILABLE.value()),
    DB_ERROR_DELETE("Error : user not deleted", HttpStatus.SERVICE_UNAVAILABLE.value()),
    DB_ERROR_GET("Error : user not found", HttpStatus.NOT_FOUND.value()),
    BOOK_NOT_FOUND("Book not found", HttpStatus.NOT_FOUND.value()),
    REPORT_NOT_FOUND("Report not found",HttpStatus.NOT_FOUND.value()),
    REPORT_ERROR_CREATION("Report not created",HttpStatus.SERVICE_UNAVAILABLE.value()),
    REPORT_ERROR_DELETE("report not deleted",HttpStatus.SERVICE_UNAVAILABLE.value());

    String message;
    int code;

    ErrorType(String message, int code) {
        this.message = message;
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public int getCode() {
        return code;
    }
}
