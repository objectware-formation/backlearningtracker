package fr.objectware.learningtracker.errors;

import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.www.BasicAuthenticationEntryPoint;

import java.io.IOException;
import java.io.PrintWriter;

public class UserAuthenticationErrorHandler extends BasicAuthenticationEntryPoint {
    private final ObjectMapper mapper;

    public UserAuthenticationErrorHandler() {
        this.mapper = new ObjectMapper();
    }

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException {
        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        response.addHeader("WWW-Authenticate", "Basic realm=" + getRealmName() + "");
        final PrintWriter writer = response.getWriter();
        writer.println(ErrorType.CONNEXION_REFUSED);    }
}
