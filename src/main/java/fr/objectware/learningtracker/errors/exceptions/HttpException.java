package fr.objectware.learningtracker.errors.exceptions;

import fr.objectware.learningtracker.errors.ErrorType;

public class HttpException extends Exception {

    private int code;
    public HttpException(ErrorType error) {
        super(error.getMessage());
        this.code = error.getCode();
    }

    public int getCode() {
        return code;
    }
}
