package fr.objectware.learningtracker.services;

import fr.objectware.learningtracker.controllers.books.forms.BooksFormCreate;
import fr.objectware.learningtracker.controllers.books.forms.BooksFormUpdate;
import fr.objectware.learningtracker.controllers.books.forms.IBookModel;
import fr.objectware.learningtracker.controllers.tags.forms.TagsFormCreate;
import fr.objectware.learningtracker.errors.ErrorType;
import fr.objectware.learningtracker.errors.exceptions.HttpException;
import fr.objectware.learningtracker.models.BooksModel;
import fr.objectware.learningtracker.models.TagsModel;
import fr.objectware.learningtracker.models.UsersModel;
import fr.objectware.learningtracker.repositories.BooksRepository;
import fr.objectware.learningtracker.repositories.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class BooksService {
    @Autowired
    private BooksRepository booksRepository;

    @Autowired
    private TagsService tagsService;

    @Autowired
    private UsersRepository usersRepository;
    @Autowired
    private UsersService usersService;

    public BooksModel createBook(BooksFormCreate book) throws HttpException {
        UsersModel auth = (UsersModel) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Optional<UsersModel> user = usersRepository.findByUuid(auth.getUuid());


        if(user.isEmpty()){
            throw new HttpException(ErrorType.USER_NOT_FOUND);
        }

        try {
            BooksModel booksModel = new BooksModel(
                    book.getName(),
                    user.get().getUuid().toString(),
                    user.get().getUsername(),
                    book.getDescription(),
                    book.getSellLink(),
                    book.getStatus()
            );
            if (book.getTags()  != null) {
                booksModel = addTagsOrCreateToBook(book, booksModel);
            }
            return booksRepository.insert(booksModel);

        } catch (HttpException e) {
            System.out.println(e.getMessage());
            throw new HttpException(ErrorType.BOOK_NOT_FOUND);
        }
    }

    public BooksModel getBook(String name)throws HttpException {
        Optional<BooksModel> book = booksRepository.findByName(name);
        if (book.isPresent()){
           return  book.get();
        }
        throw new HttpException(ErrorType.BOOK_NOT_FOUND);

    }
    public BooksModel updateBookStatus(String uuid, BooksFormUpdate book) throws Exception {
        Optional<BooksModel> bookToUpdate = booksRepository.findByUuid(UUID.fromString(uuid));

        if(bookToUpdate.isEmpty()){
            throw new HttpException(ErrorType.BOOK_NOT_FOUND);
        }

        bookToUpdate.get().setStatus(book.getStatus());
        booksRepository.save(bookToUpdate.get());
        return bookToUpdate.get();
    }

    public BooksModel updateBookTags(String uuid, BooksFormUpdate book) throws Exception {

        try {
            Optional<BooksModel> bookModel = booksRepository.findByUuid(UUID.fromString(uuid));
            if(bookModel.isEmpty()){
                throw new HttpException(ErrorType.BOOK_NOT_FOUND);
            }

            if(book.getTags() != null){
                addTagsOrCreateToBook(book, bookModel.get());
            }

            return booksRepository.save(bookModel.get());
        }catch (Exception e) {
            throw new HttpException(ErrorType.BOOK_NOT_FOUND);
        }
    }

    public List<BooksModel> getAllBooks() throws Exception {
        return booksRepository.findAll();
    }

    public void deleteBook(String id) throws Exception{
        booksRepository.deleteById(id);
    }


    public List<BooksModel> getPreferredBooks() throws Exception{
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        UserDetails userDetails = (UserDetails) auth.getPrincipal();
        String name = userDetails.getUsername();
        UsersModel usersModel = usersService.getUserByName(name);
        List<BooksModel> books = new ArrayList<>();
        List<TagsModel> tags = new ArrayList<>();
        Optional<UsersModel> opUser = usersRepository.findByUuid(usersModel.getUuid());

        if(opUser.isPresent()){
            UsersModel userModel =  opUser.get();
            for(String bookId : userModel.getBooksIds().keySet()){
                Optional<BooksModel> bookModel = booksRepository.findById(bookId);

                if(bookModel.isEmpty()){
                    continue;
                }

                for(UUID tagUUID : Objects.requireNonNull(bookModel.get().getTags())){
                    Optional<TagsModel> tag = tagsService.getTagByUuid(tagUUID);
                    tag.ifPresent(tags::add);
                }

            }
            for(BooksModel book : this.getAllBooks()){
                if(book.getTags() == null){
                    continue;
                }
                for(UUID tagUuid : book.getTags()){
                    if(tagUuid != null && !tags.contains(tagUuid)){
                        books.add(book);
                    }
                }
            }
            return books;
        }else{
            return this.getAllBooks().subList(0, 2);
        }

    }

    public BooksModel addTagsOrCreateToBook(IBookModel dto, BooksModel book) throws HttpException {

        List<TagsFormCreate> tags = dto.getTags();

        for(TagsFormCreate tag : tags){
            Optional<TagsModel> tagsModel = tagsService.getTagByName(tag.getName());
            if(tagsModel.isEmpty()){
                tagsService.create(
                        new TagsModel(
                                tag.getName(),
                                tag.getAuthor()
                        ));
            }
            book.addTags(tagsModel.get().getUuid());
        }


        return book;
    }
}
