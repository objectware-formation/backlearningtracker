package fr.objectware.learningtracker.services;

import fr.objectware.learningtracker.controllers.reports.forms.ReportsFormCreate;
import fr.objectware.learningtracker.errors.ErrorType;
import fr.objectware.learningtracker.errors.exceptions.HttpException;
import fr.objectware.learningtracker.models.ReportsModel;
import fr.objectware.learningtracker.models.UsersModel;
import fr.objectware.learningtracker.repositories.ReportsRepository;
import fr.objectware.learningtracker.repositories.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Optional;
import java.util.UUID;

@Service
public class ReportsService {

    @Autowired
    private ReportsRepository reportsRepository;
    @Autowired
    private UsersRepository usersRepository;

    public ReportsModel addReport(ReportsFormCreate reportDto)throws HttpException{

        try{
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();

            UUID uuid = usersRepository.findByUsername(auth.getName()).getUuid();
            ReportsModel report = new ReportsModel(uuid,reportDto.getTarget(), reportDto.getDescription()) ;

            reportsRepository.save(report);
            return report;
        }catch(Exception e){
            throw new HttpException(ErrorType.REPORT_ERROR_CREATION);
        }
    }

    public Iterable<ReportsModel> getReports()throws HttpException {
        try{
            return new ArrayList<>(reportsRepository.findAll());
        }catch(Exception e){
            throw new HttpException(ErrorType.REPORT_NOT_FOUND);
        }
    }

    public ReportsModel getReport(String id)throws HttpException{
        Optional<ReportsModel> report = reportsRepository.findById(id);
        if(report.isPresent()){
            return report.get();
        }
        throw new HttpException(ErrorType.REPORT_NOT_FOUND);
    }

    public ReportsModel deleteReport(String id ) throws HttpException{
        Optional<ReportsModel> report = reportsRepository.findById(id);
        if(report.isPresent()){
            reportsRepository.delete(report.get());
            return report.get();
        }
        throw new HttpException(ErrorType.REPORT_ERROR_DELETE);

    }




}
