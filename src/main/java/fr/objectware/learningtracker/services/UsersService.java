package fr.objectware.learningtracker.services;

import fr.objectware.learningtracker.controllers.users.dto.UsersDtoUpdate;
import fr.objectware.learningtracker.mappers.UsersMapper;
import fr.objectware.learningtracker.models.UsersModel;
import fr.objectware.learningtracker.repositories.UsersRepository;
import fr.objectware.learningtracker.errors.ErrorType;
import fr.objectware.learningtracker.errors.exceptions.HttpException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Optional;
import java.util.UUID;

@Service
public class UsersService {
        @Autowired
        private UsersMapper mapper;
        @Autowired
        private UsersRepository repository;


        public UsersModel update(String id, UsersDtoUpdate userDto)throws HttpException{

            Optional<UsersModel> user = repository.findById(id) ;
            if(user.isPresent())
            {
                UsersModel u = user.get();
                mapper.updateUserFromDto(userDto,u);
                repository.save(u);
                return u;

            }
            throw new HttpException(ErrorType.USER_NOT_FOUND);

        }

        public Iterable<UsersModel> getUsers()throws HttpException {
            try{
                return new ArrayList<>(repository.findAll());
            }catch(Exception e){
                throw new HttpException(ErrorType.DB_ERROR_GET);
            }

        }

    public UsersModel getUser(String uuid) throws HttpException{
        Optional<UsersModel> user = repository.findByUuid(UUID.fromString(uuid));
        if(user.isPresent()) {
            return user.get();

        }
       throw new HttpException(ErrorType.USER_NOT_FOUND);
    }

    public UsersModel updateAllField(String id,UsersModel model)throws HttpException {
        Optional<UsersModel> u = repository.findById(id) ;
        if(u.isPresent()){
            UsersModel user = u.get();
            user.setUsername(model.getUsername());
            user.setEmail(model.getEmail());
            user.setPassword(model.getPassword());
            user.setRole(model.getRole());
            return user;

        }
       throw new HttpException(ErrorType.USER_NOT_FOUND);
    }

    public UsersModel deleteUser(String id) throws HttpException {
        Optional<UsersModel> u = repository.findById(id) ;
        if(u.isPresent()){
            UsersModel user = u.get();
            repository.deleteById(id);
            return  user;
        }
        throw new HttpException(ErrorType.DB_ERROR_DELETE);
    }


    public String getUsernameByUUID(String uuid) throws HttpException {
            try {

                Optional<UsersModel> user = repository.findByUuid(UUID.fromString(uuid));

                if(user.isEmpty()){
                    throw new HttpException(ErrorType.USER_NOT_FOUND);
                }

                return user.get().getUsername();
            } catch (Exception e) {
                throw new HttpException(ErrorType.DB_ERROR_GET);
            }
    }

    public UsersModel getUserByName(String name) throws HttpException{
        return repository.findByUsername(name);
    }
}


