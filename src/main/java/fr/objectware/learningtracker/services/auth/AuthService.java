package fr.objectware.learningtracker.services.auth;

import fr.objectware.learningtracker.config.JwtTokenUtils;
import fr.objectware.learningtracker.config.enums.Roles;
import fr.objectware.learningtracker.controllers.auth.dto.LoginDto;
import fr.objectware.learningtracker.controllers.auth.dto.RegisterDto;
import fr.objectware.learningtracker.controllers.auth.responses.AuthResponse;
import fr.objectware.learningtracker.models.UsersModel;
import fr.objectware.learningtracker.repositories.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class AuthService {

    @Autowired
    private UsersRepository usersRepository;

    @Autowired
    private JwtTokenUtils jwtService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public AuthResponse register(RegisterDto request) {
        UsersModel model = new UsersModel(
                request.getUsername(),
                request.getEmail(),
                passwordEncoder.encode(request.getPassword()),
                Roles.DEFAULT
        );

        usersRepository.insert(model);
        String token = jwtService.generateToken(model);
        return new AuthResponse(token);
    }

    public AuthResponse login(LoginDto request) {
        UsersModel model = usersRepository.findByUsername(request.getUsername());

        if(!passwordEncoder.matches(request.getPassword(), model.getPassword())) {
            throw new RuntimeException("Invalid password");
        }

        String token = jwtService.generateToken(model);
        return new AuthResponse(token);
    }
}
