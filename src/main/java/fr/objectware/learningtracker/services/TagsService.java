package fr.objectware.learningtracker.services;

import fr.objectware.learningtracker.models.TagsModel;
import fr.objectware.learningtracker.repositories.TagsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class TagsService {
    @Autowired
    private TagsRepository tagsRepository;

    public TagsModel create(String name, String author) {
        return tagsRepository.insert(new TagsModel(name, author));
    }

    public TagsModel create(TagsModel tag) {
        return tagsRepository.insert(tag);
    }



    public TagsModel findOrCreate(String name, String author) {
        Optional<TagsModel> tag = tagsRepository.findByName(name);
        return tag.orElseGet(() -> tagsRepository.save(new TagsModel(name, author)));
    }
    public List<TagsModel> getAllTags() {
        return tagsRepository.findAll();
    }

    public Optional<TagsModel> getTagById(String id) {
        return tagsRepository.findById(id);
    }
    public Optional<TagsModel> getTagByUUID(UUID uuid) {
        return tagsRepository.findByUuid(uuid);
    }


    public Optional<TagsModel> getTagByName(String name) {
        return tagsRepository.findByName(name);
    }

    public List<TagsModel> getTagByNames(List<TagsModel> names) {
        return tagsRepository.findByNameIn(
                names.stream().map(TagsModel::getName).toList()
        );
    }

    public Optional<TagsModel> getTagByUuid(UUID uuid) {
        return tagsRepository.findByUuid(uuid);
    }

    public void deleteTagById(String id) throws Exception {
        try {
            tagsRepository.deleteById(id);
        } catch (Exception e) {
            throw new Exception("Error deleting tag");
        }
    }
}
