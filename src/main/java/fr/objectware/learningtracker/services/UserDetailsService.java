package fr.objectware.learningtracker.services;

import fr.objectware.learningtracker.errors.exceptions.HttpException;
import fr.objectware.learningtracker.models.UsersModel;
import fr.objectware.learningtracker.repositories.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserDetailsService implements org.springframework.security.core.userdetails.UserDetailsService {
    @Autowired
    private UsersRepository usersRepository;

    @Override
    public UsersModel loadUserByUsername(String id) {
        try {
            Optional<UsersModel> model = usersRepository.findById(id);

            if(model.isEmpty()) {
                throw new Exception("User not found");
            }

            return model.get();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
