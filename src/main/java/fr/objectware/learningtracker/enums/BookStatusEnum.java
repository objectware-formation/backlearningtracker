package fr.objectware.learningtracker.enums;

public enum BookStatusEnum {
    TO_READ,
    READING,
    READ
}
