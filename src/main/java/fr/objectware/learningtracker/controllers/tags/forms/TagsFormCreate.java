package fr.objectware.learningtracker.controllers.tags.forms;

import fr.objectware.learningtracker.models.TagsModel;

public class TagsFormCreate extends TagsModel {
    public TagsFormCreate(String name, String author) {
        super(name, author);
    }
}
