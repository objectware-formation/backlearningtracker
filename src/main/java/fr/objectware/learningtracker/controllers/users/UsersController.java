package fr.objectware.learningtracker.controllers.users;

import fr.objectware.learningtracker.controllers.users.dto.UsersDtoUpdate;

import fr.objectware.learningtracker.controllers.users.responses.UsernameResponse;
import fr.objectware.learningtracker.models.UsersModel;
import fr.objectware.learningtracker.services.UsersService;
import fr.objectware.learningtracker.errors.exceptions.HttpException;
import org.bson.json.JsonObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/users")
public class UsersController {
    @Autowired
    private UsersService service;

    @GetMapping
        public ResponseEntity<Iterable<UsersModel>> getAll(){
        try{
            return new ResponseEntity<>(service.getUsers(),HttpStatus.OK);
        }catch(HttpException e){
            return new ResponseEntity<>(HttpStatus.valueOf(e.getCode()));
        }

        }
    @GetMapping("/find/{uuid}")
    public ResponseEntity<UsersModel> getUser(@PathVariable String uuid){
        try {
            UsersModel user = service.getUser(uuid);
            return  new ResponseEntity<>(user, HttpStatus.OK);
        }catch(HttpException e){
            System.out.println(e.getMessage());
            return new ResponseEntity<>(HttpStatus.valueOf(e.getCode()));
        }

    }

    @PatchMapping("/{id}")
    public ResponseEntity<UsersModel> update(@PathVariable String id, @RequestBody UsersDtoUpdate model){
        try{
            UsersModel user = service.update(id,model);
            return new ResponseEntity<>(user,HttpStatus.OK);
        }catch(HttpException e){
            return new ResponseEntity<>(HttpStatus.valueOf(e.getCode()));
        }


    }

    @PutMapping("/{id}")
    public ResponseEntity<UsersModel> updateAllField(@PathVariable String id, @RequestBody UsersModel model){
        try {
            UsersModel user = service.updateAllField(id,model);
            return new ResponseEntity<>(user,HttpStatus.OK);
        }catch (HttpException e){
            return new ResponseEntity<>(HttpStatus.valueOf(e.getCode()));
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<UsersModel> deleteUser(@PathVariable String id){
        try{
            UsersModel user = service.deleteUser(id);
            return new ResponseEntity<>(user,HttpStatus.OK);
        }catch(HttpException e){
            return new ResponseEntity<>(HttpStatus.valueOf(e.getCode()));
        }
    }

    @GetMapping("/name/{uuid}")
    public ResponseEntity<Object> getUsernameByUuid(@PathVariable String uuid){
        try {
            UsernameResponse username = new UsernameResponse(service.getUsernameByUUID(uuid));
            return  new ResponseEntity<>(username, HttpStatus.OK);
        }catch(HttpException e){
            System.out.println(e.getMessage());
            return new ResponseEntity<>(HttpStatus.valueOf(e.getCode()));
        }

    }
}
