package fr.objectware.learningtracker.controllers.users.dto;

public class UsersDtoCreate {
    private String username;
    private String email;
    private String password;

    public UsersDtoCreate(String username, String email, String password) {
        this.username = username;
        this.email = email;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }
}
