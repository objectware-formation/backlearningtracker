package fr.objectware.learningtracker.controllers.users.responses;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UsernameResponse {

    @JsonProperty("username")
    private String username;

    public UsernameResponse(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }
}
