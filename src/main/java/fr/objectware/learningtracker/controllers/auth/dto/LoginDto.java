package fr.objectware.learningtracker.controllers.auth.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class LoginDto {

    private String username;
    private String password;

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

}
