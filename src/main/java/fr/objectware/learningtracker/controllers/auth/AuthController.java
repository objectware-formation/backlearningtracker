package fr.objectware.learningtracker.controllers.auth;


import fr.objectware.learningtracker.controllers.auth.dto.LoginDto;
import fr.objectware.learningtracker.controllers.auth.dto.RegisterDto;
import fr.objectware.learningtracker.controllers.auth.responses.AuthResponse;
import fr.objectware.learningtracker.services.auth.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/auth")
public class AuthController {

    @Autowired
    private AuthService authService;

    @PostMapping("/login")
    public AuthResponse login(@RequestBody LoginDto dto) {
        return authService.login(dto);
    }

    @PostMapping("/register")
    public AuthResponse register(@RequestBody RegisterDto dto) {
        return authService.register(dto);
    }
}
