package fr.objectware.learningtracker.controllers.reports.forms;

import org.springframework.data.annotation.Id;

public class ReportsFormCreate {

    private String target;
    private String description;

    public ReportsFormCreate(String target,String description){

        this.target=target;
        this.description=description;
    }


    public String getTarget() {
        return target;
    }

    public String getDescription() {
        return description;
    }
}
