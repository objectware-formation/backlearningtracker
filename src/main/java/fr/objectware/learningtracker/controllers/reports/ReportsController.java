package fr.objectware.learningtracker.controllers.reports;

import fr.objectware.learningtracker.controllers.reports.forms.ReportsFormCreate;
import fr.objectware.learningtracker.errors.exceptions.HttpException;
import fr.objectware.learningtracker.models.ReportsModel;
import fr.objectware.learningtracker.models.UsersModel;
import fr.objectware.learningtracker.services.ReportsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/reports")
public class ReportsController {

    @Autowired
    private ReportsService reportsService;

    @PostMapping("/create")
    public ResponseEntity<ReportsModel> createReport(@RequestBody ReportsFormCreate reportDto){
        try{

            ReportsModel report = reportsService.addReport(reportDto);
            return new ResponseEntity<>(report,HttpStatus.CREATED);
        }catch (HttpException e){
            return new ResponseEntity<>(HttpStatus.valueOf(e.getCode()));
        }
    }
    @GetMapping
    public ResponseEntity<Iterable<ReportsModel>> getAll(){
        try{
            return new ResponseEntity<>(reportsService.getReports(), HttpStatus.OK);
        }catch(HttpException e){
            return new ResponseEntity<>(HttpStatus.valueOf(e.getCode()));
        }

    }
    @GetMapping("/{id}")
    public ResponseEntity<ReportsModel> getUser(@PathVariable String id){
        try {
            ReportsModel report = reportsService.getReport(id);
            return  new ResponseEntity<>(report, HttpStatus.OK);
        }catch(HttpException e){
            return new ResponseEntity<>(HttpStatus.valueOf(e.getCode()));
        }

    }

    @DeleteMapping("/{id}")
    public ResponseEntity<ReportsModel> deleteUser(@PathVariable String id){
        try{
            ReportsModel report= reportsService.deleteReport(id);
            return new ResponseEntity<>(report,HttpStatus.OK);
        }catch(HttpException e){
            return new ResponseEntity<>(HttpStatus.valueOf(e.getCode()));
        }
    }

}
