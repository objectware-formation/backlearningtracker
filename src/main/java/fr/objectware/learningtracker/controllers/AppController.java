package fr.objectware.learningtracker.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class AppController {

    @GetMapping
    public String index() {
        return "Hello World";
    }

    @GetMapping("/test")
    public String test() {
        return "Hello World";
    }
}
