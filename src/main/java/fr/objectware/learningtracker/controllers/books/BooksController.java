package fr.objectware.learningtracker.controllers.books;

import fr.objectware.learningtracker.controllers.books.forms.BooksFormCreate;
import fr.objectware.learningtracker.controllers.books.forms.BooksFormUpdate;
import fr.objectware.learningtracker.errors.exceptions.HttpException;
import fr.objectware.learningtracker.models.BooksModel;
import fr.objectware.learningtracker.services.BooksService;
import fr.objectware.learningtracker.services.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/books")
public class BooksController {
    @Autowired
    private BooksService booksService;

     @GetMapping
    public ResponseEntity<List<BooksModel>> getAllBooks() {
         try {
             System.out.println("get all books");
             List<BooksModel> books = booksService.getAllBooks();
             return new ResponseEntity<>(books, HttpStatus.OK);
         } catch (Exception e) {
             return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
         }
     }
     @GetMapping("/{name}")
     public ResponseEntity<BooksModel> getBook(@PathVariable String name){
         try{
             BooksModel book = booksService.getBook(name);
             return new ResponseEntity<>(book,HttpStatus.OK);
         }catch(HttpException e){
             return new ResponseEntity<>(HttpStatus.valueOf(e.getCode()));
         }

     }
    @PostMapping("/create")
    public ResponseEntity<Object> createBook(
            @RequestBody BooksFormCreate book
    ) {


        try {
             BooksModel newBook = booksService.createBook(book);
             return new ResponseEntity<>(newBook, HttpStatus.CREATED);
        } catch (HttpException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatusCode.valueOf(e.getCode()));
        }
    }

    @PatchMapping("/updateStatus")
    public ResponseEntity<BooksModel> updateBookStatus(
            @RequestParam String uuid,
            @RequestBody BooksFormUpdate book
    ) {
        try {
            BooksModel updatedBook = booksService.updateBookStatus(uuid, book);
            return new ResponseEntity<>(updatedBook, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }



    @DeleteMapping("/{id}")
    public ResponseEntity<BooksModel> deleteBookByUser(@PathVariable("id") String id){
        try{
            booksService.deleteBook(id);
            return new ResponseEntity<>(HttpStatus.OK);
        }catch(Exception e){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PatchMapping("/update-tags/{uuid}")
    public ResponseEntity<BooksModel> updateBookTags(
            @PathVariable("uuid") String uuid,
            @RequestBody BooksFormUpdate book
    ) {
        try {
            BooksModel updatedBook = booksService.updateBookTags(uuid, book);
            return new ResponseEntity<>(updatedBook, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/preferences")
    public ResponseEntity<List<BooksModel>> getPreferredBooks(){
        try{
            List<BooksModel> books = new ArrayList<>();
            books = booksService.getPreferredBooks();
            System.out.println(books);
            return new ResponseEntity<>(books, HttpStatus.OK);
        }catch(Exception e){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
