package fr.objectware.learningtracker.controllers.books.forms;

import fr.objectware.learningtracker.controllers.tags.forms.TagsFormCreate;
import fr.objectware.learningtracker.enums.BookStatusEnum;

import java.util.List;

public interface IBookModel {
    String getName();
    String getAuthorName();
    String getAuthorUUID();
    String getDescription();
    String getSellLink();
    BookStatusEnum getStatus();
    List<TagsFormCreate> getTags();
}
