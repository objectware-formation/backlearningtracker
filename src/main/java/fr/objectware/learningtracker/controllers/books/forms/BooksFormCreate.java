package fr.objectware.learningtracker.controllers.books.forms;

import fr.objectware.learningtracker.controllers.tags.forms.TagsFormCreate;
import fr.objectware.learningtracker.enums.BookStatusEnum;
import fr.objectware.learningtracker.models.TagsModel;
import org.springframework.lang.Nullable;

import java.util.List;
import java.util.UUID;

public class BooksFormCreate implements IBookModel{
    private String name;

    private String authorName;
    private String authorUUID;
    @Nullable
    private String description;
    @Nullable
    private String sellLink;


    @Nullable
    private BookStatusEnum status;

    private List<TagsFormCreate> tags;

    public BooksFormCreate() {
    }

    public String getName() {
        return name;
    }

    public String getAuthorName() {
        return authorName;
    }

    public String getAuthorUUID() {
        return authorUUID;
    }

    @Nullable
    public String getDescription() {
        return description;
    }

    @Nullable
    public String getSellLink() {
        return sellLink;
    }

    @Nullable
    public BookStatusEnum getStatus() {
        return status;
    }

    @Nullable
    public List<TagsFormCreate> getTags() {
        return tags;
    }
}
