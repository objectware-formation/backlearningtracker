package fr.objectware.learningtracker.controllers.books.forms;

import fr.objectware.learningtracker.controllers.tags.forms.TagsFormCreate;
import fr.objectware.learningtracker.enums.BookStatusEnum;
import fr.objectware.learningtracker.models.TagsModel;
import org.springframework.lang.Nullable;

import java.util.List;

public class BooksFormUpdate implements IBookModel {
    @Nullable
    private String name;
    @Nullable
    private String authorName;
    @Nullable
    private String authorUUID;
    @Nullable
    private String description;
    @Nullable
    private String sellLink;
    @Nullable
    private BookStatusEnum status;

    private List<TagsFormCreate> tags;

    public String getName() {
        return name;
    }

    public String getAuthorName() {
        return authorName;
    }

    public String getAuthorUUID() {
        return authorUUID;
    }

    @org.springframework.lang.Nullable
    public String getDescription() {
        return description;
    }

    @org.springframework.lang.Nullable
    public String getSellLink() {
        return sellLink;
    }

    @org.springframework.lang.Nullable
    public BookStatusEnum getStatus() {
        return status;
    }

    @Nullable
    public List<TagsFormCreate> getTags() {
        return tags;
    }

}
