package fr.objectware.learningtracker.controllers;

import fr.objectware.learningtracker.controllers.tags.forms.TagsFormCreate;
import fr.objectware.learningtracker.models.TagsModel;
import fr.objectware.learningtracker.services.TagsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/tags")
public class TagsController {

    @Autowired
    private TagsService TagsService;

    @PostMapping("/create")
    public ResponseEntity<TagsModel> createTag(
            @RequestBody TagsFormCreate tag
    ) {
        try {
            TagsModel createdTag = TagsService.create(tag.getName(), tag.getAuthor());
            return new ResponseEntity<>(createdTag,HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping()
    public ResponseEntity<List<TagsModel>> getAllTags() {
        try {
            List<TagsModel> tags = TagsService.getAllTags();
            return new ResponseEntity<>(tags,HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<Optional<TagsModel>> getTagById(@PathVariable String id) {
        try {
            Optional<TagsModel> tag = TagsService.getTagById(id);
            return new ResponseEntity<>(tag,HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/{name}")
    public ResponseEntity<Optional<TagsModel>> getTagByName(@PathVariable String name) {
        try {
            Optional<TagsModel> tags = TagsService.getTagByName(name);
            return new ResponseEntity<>(tags,HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Boolean> deleteTag(@PathVariable String id) {
        try {
            TagsService.deleteTagById(id);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
