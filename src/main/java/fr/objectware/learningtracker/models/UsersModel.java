package fr.objectware.learningtracker.models;

import fr.objectware.learningtracker.config.enums.Roles;
import fr.objectware.learningtracker.enums.BookStatusEnum;
import fr.objectware.learningtracker.enums.States;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.*;

@Document
public class UsersModel implements UserDetails {

    @Id
    private String id;
    private UUID uuid;
    private String username;
    private String mail;
    private String password;
    private Roles role;
    private Map<String, BookStatusEnum> booksIds = new HashMap<>();


    public UsersModel(String username, String mail, String password, Roles role) {
        this.username = username;
        this.mail = mail;
        this.password = password;
        this.role = role;
        this.uuid = UUID.randomUUID();

    }

    public String getId() {
        return id;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return List.of(new SimpleGrantedAuthority(role.name()));
    }

    public UUID getUuid() {
        return uuid;
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return mail;
    }

    public void setEmail(String email) {
        this.mail = email;
    }


    public void setPassword(String password) {
        this.password = password;
    }
    public Roles getRole() {
        return role;
    }

    public void setRole(Roles role) {
        this.role = role;
    }

    public Map<String, BookStatusEnum> getBooksIds() {
        return booksIds;
    }

    public void addBook(String idBook, BookStatusEnum state) {
        this.booksIds.put(idBook,state);
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }


    public void setId(String id) {this.id = id;}
}
