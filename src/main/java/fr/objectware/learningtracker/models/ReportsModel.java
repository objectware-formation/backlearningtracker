package fr.objectware.learningtracker.models;

import org.springframework.data.annotation.Id;

import java.util.UUID;

public class ReportsModel {
    @Id
    private String id;

    private UUID author;
    private String target;

    private String description;

    public ReportsModel(UUID author, String target, String description){
        this.author = author;
        this.target=target;
        this.description= description;
    }
    public String getId() {
        return id;
    }
    public UUID getAuthor() {
        return this.author;
    }

    public String getTarget() {
        return this.target;
    }

    public String getDescription() {
        return this.description;
    }


}
