package fr.objectware.learningtracker.models;

import com.mongodb.lang.Nullable;
import fr.objectware.learningtracker.enums.BookStatusEnum;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Document(collection = "books")
public class BooksModel {
    @Id
    private String id;

    private UUID uuid;
    private String name;
    @Nullable
    private String description;
    @Nullable
    private String sellLink;
    private String authorUUID;

    private String authorName;

    private List<UUID> tags = new ArrayList<>();


    @Nullable
    private BookStatusEnum status;
    public BooksModel(
            String name,
            @Nullable String authorUUID,
            @Nullable String authorName,
            @Nullable String description,
            @Nullable String sellLink,
            @Nullable BookStatusEnum status
    ) {
        this.name = name;
        this.authorUUID = authorUUID;
        this.authorName = authorName;
        this.description = description;
        this.sellLink = sellLink;
        this.uuid = UUID.randomUUID();
        this.status = status;
        this.tags = tags;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Nullable
    public String getDescription() {
        return description;
    }

    public void setDescription(@Nullable String description) {
        this.description = description;
    }

    @Nullable
    public String getSellLink() {
        return sellLink;
    }

    public void setSellLink(@Nullable String sellLink) {
        this.sellLink = sellLink;
    }

    public String getAuthorName() {
        return authorName;
    }

    public String getAuthorUUID() {
        return authorUUID;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public void setAuthorName(String author) {
        this.authorName = author;
    }

    public void setAuthorUUID(String authorUUID) {
        this.authorUUID = authorUUID;
    }

    @Nullable
    public BookStatusEnum getStatus() {
        return status;
    }

    public void setStatus(@Nullable BookStatusEnum status) {
        this.status = status;
    }

    @Nullable
    public List<UUID> getTags() {
        return tags;
    }

    public void addTags(UUID tag) {
        this.tags.add(tag);
    }
}
