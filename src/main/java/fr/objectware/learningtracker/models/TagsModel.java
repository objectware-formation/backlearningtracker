package fr.objectware.learningtracker.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.UUID;

@Document(collection = "tags")
public class TagsModel {
    @Id
    private String id;

    private String name;

    private String author;

    private UUID uuid;

    public TagsModel(String name, String author) {
        this.name = name;
        this.author = author;
        this.uuid = UUID.randomUUID();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public UUID getUuid() {
        return uuid;
    }
}
