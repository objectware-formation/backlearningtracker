package fr.objectware.learningtracker.repositories;

import fr.objectware.learningtracker.models.BansModel;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface BansRepository extends MongoRepository<BansModel, String> {
}
