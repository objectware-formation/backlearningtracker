package fr.objectware.learningtracker.repositories;

import fr.objectware.learningtracker.models.UsersModel;
import org.springframework.data.mongodb.repository.MongoRepository;

import javax.swing.text.html.Option;
import java.util.Optional;
import java.util.UUID;

public interface UsersRepository extends MongoRepository<UsersModel, String> {
    UsersModel findByUsername(String username);


    Optional<UsersModel> findById(String s);

    Optional<UsersModel> findByUuid(UUID uuid);




}
