package fr.objectware.learningtracker.repositories;

import com.mongodb.lang.NonNullApi;
import fr.objectware.learningtracker.models.ReportsModel;
import fr.objectware.learningtracker.models.UsersModel;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface ReportsRepository extends MongoRepository<ReportsModel,String> {
    Optional<ReportsModel> findById(String s);

}
