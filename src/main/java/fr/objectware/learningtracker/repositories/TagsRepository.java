package fr.objectware.learningtracker.repositories;

import fr.objectware.learningtracker.models.TagsModel;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface TagsRepository extends MongoRepository<TagsModel, String> {
    Optional<TagsModel> findByName(String name);

    List<TagsModel> findByNameIn(List<String> names);

    Optional<TagsModel> findByUuid(UUID uuid);
}
