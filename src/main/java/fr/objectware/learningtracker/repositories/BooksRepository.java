package fr.objectware.learningtracker.repositories;

import fr.objectware.learningtracker.models.BooksModel;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.Aggregation;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface BooksRepository extends MongoRepository<BooksModel, String> {

    Optional<BooksModel> findById(String id);
    Optional<BooksModel> findByName(String name);

    Optional<BooksModel> findByUuid(UUID uuid);

    List<BooksModel> findAllByName(String name);
}
